plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroid)
}

android {
    compileSdkVersion(AndroidSDK.compile)
    buildToolsVersion(CommonVersions.androidBuildToolsVersion)

    defaultConfig {
        applicationId = "com.sergeybogdanec.accounting"
        minSdkVersion(AndroidSDK.min)
        targetSdkVersion(AndroidSDK.target)
        versionCode = CommonVersions.accountingModuleVersionCode
        versionName = CommonVersions.accountingModuleVersionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = CommonVersions.jvmTarget
    }
}

dependencies {
    implementation(project(Modules.Features.auth))
    implementation(project(Modules.Features.settings))

    implementation(Libraries.kotlin)
    implementation(Libraries.androidxCore)
    implementation(Libraries.appCompat)
    implementation(Libraries.material)
    implementation(Libraries.constraintsLayout)

    testImplementation(TestLibraries.junit)
    androidTestImplementation(TestLibraries.androidJunit)
    androidTestImplementation(TestLibraries.espresso)
}